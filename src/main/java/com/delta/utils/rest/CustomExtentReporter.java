package com.delta.utils.rest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import cucumber.api.Scenario;

/**
 * 
 * @author Sreekanth
 *
 */

public class CustomExtentReporter {
	private ExtentHtmlReporter extentHtmlreporter;
	private ExtentReports extentReports;
	
	public CustomExtentReporter(String reploc) {
		extentHtmlreporter = new ExtentHtmlReporter(reploc);
		extentReports = new ExtentReports();
		extentReports.attachReporter(extentHtmlreporter);
	}
	
	public void createTest(Scenario scenario) {
		String scName = getScenarioTitle(scenario);
		if(scenario !=null) {
			System.out.println("SREEKANTH "+scenario.getStatus());
			switch(scenario.getStatus()) {
			case PASSED:
				extentReports.createTest(scName).pass("PASSED");
			break;
			
			case FAILED:
				extentReports.createTest(scName).fail("FAILED");
			break;
			
			default:
				break;
			}
				
		}
	}
	
	public String getScenarioTitle(Scenario scenario) {
		return scenario.getName();
	}
	
	public void writeToReport() {
		if(extentReports!=null) {
			extentReports.flush();
		}
	}

}
