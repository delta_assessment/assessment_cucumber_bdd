package com.delta.utils.rest;

/*
 * @author - Sreekanth
 * 
 */

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import static io.restassured.RestAssured.given;

public class RestAPIRequestImpl {
	Response response = null;

	private static Logger LOGGER = Logger.getLogger(RestAPIRequestImpl.class);

	/**
	 * Method for POST call
	 * @author Sreekanth
	 * @param json
	 * @param url
	 * @return
	 */
	public Response post(String json, String url) {
		response = given().headers("Content-Type", "application/json").contentType(ContentType.JSON).body(json)
				.post(url).then().extract().response();
		LOGGER.info(response.getBody().asString());
		return response;
	}

	/**
	 * Method for patch call
	 * @author Sreekanth
	 * @param json
	 * @param url
	 * @return
	 */
	public Response patch(String json, String url) {
		response = given().headers("Content-Type", "application/json").contentType(ContentType.JSON).body(json)
				.patch(url).then().extract().response();
		LOGGER.info(response.getBody().asString());
		return response;
	}

	/**
	 * Method for get call
	 * @author Sreekanth
	 * @param url
	 * @return
	 */
	public Response get(String url) {
		response = given().headers("Content-Type", "application/json").get(url).then().extract().response();
		LOGGER.info(response.getBody().asString());
		return response;
	}

	/**
	 * Method for delete call
	 * @author Sreekanth
	 * @param url
	 * @return
	 */
	public Response delete(String url) {
		response = given().headers("Content-Type", "application/json").delete(url).then().extract().response();
		LOGGER.info(response.getBody().asString());
		return response;
	}
}
