package com.delta.utils.rest;


public class UrlResources {

	private static String url = ExecutionUtils.getUrl();
	public static final String CREATE_USERS = url + "api/users";

	/**
	 * 
	 * @return
	 */
	public static String getCreateUsers() {
		return CREATE_USERS;
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getUsers() {
		return url+"api/users?page=1";
	}


}
