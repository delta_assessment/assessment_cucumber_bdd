package com.delta.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

import java.io.File;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.vimalselvam.cucumber.listener.Reporter;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = { "pretty", "html:target/reports/cucumber-html-report", "json:target/cucumber-reports/Cucumber.json"},
		features = "src/test/java/com/delta/features",
		glue = "com.delta.steps",
		monochrome = true,
		strict = true)

public class CucumberRunner {}
