Feature: Test

  Scenario Outline: Create users in reqres test api
    Given Create below users <name> and <job> in reqres api
    Then response should be CREATED

    Examples: 
      | name  | job  |
      | Test1 | job1 |
      
  Scenario: Get list of users in regres api
    Given Get list of users in regres api
    Then response should be SUCCESS
    
  Scenario: Get list of users in regres api Negative Case
    Given Get list of users in regres api
    Then response should be CREATED