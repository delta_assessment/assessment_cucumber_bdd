package com.delta.steps;

import org.junit.Assert;

import com.delta.model.AddUser;
import com.delta.test.TestBase;
import com.delta.utils.rest.CustomExtentReporter;
import com.delta.utils.rest.UrlResources;

import cucumber.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;

public class TestApi extends TestBase{
	Response response = null;
	private static CustomExtentReporter extenthtmlreporter;
	private static boolean isExtentRunning;
	
	@Before
	public void isExtentRunningCheck() {
		if(!isExtentRunning) {
			extenthtmlreporter = new CustomExtentReporter(System.getProperty("user.dir")+"\\target\\extent-report.html");
			isExtentRunning=true;
		}
	}
	
	@After
	public void AfterScenario(Scenario scenario) {
		extenthtmlreporter.createTest(scenario);
		extenthtmlreporter.writeToReport();
	}
	
	@Given("Create below users (.+) and (.+) in reqres api")
	public void createUsers(String name, String job) {
		AddUser adduser = new AddUser();
		adduser.setName(name);
		adduser.setJob(job);
		response = request.post(getJson(adduser), UrlResources.getCreateUsers());
		System.out.println("Response String -" + response.getBody().asString());
	}
	
	@Then("response should be (.+)")
	public void validateResponse(String statusCode) {
		if(statusCode.equalsIgnoreCase("CREATED"))
			Assert.assertEquals(CREATED, response.getStatusCode());
		else if (statusCode.equalsIgnoreCase("SUCCESS"))
			Assert.assertEquals(SUCCESS, response.getStatusCode());
	}
	
	@Given("Get list of users in regres api")
	public void getListOfUsers() {
		response = request.get(UrlResources.getUsers());
		System.out.println("Response String -" + response.getBody().asString());
	}
	

}
