package com.delta.test;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.delta.utils.rest.RestAPIRequestImpl;
import io.restassured.internal.RestAssuredResponseImpl;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TestBase implements TestConstants {
	public static Logger logger = Logger.getLogger(TestBase.class);
	Gson gson = new GsonBuilder().create();
	Properties properties = new Properties();
	protected Response response;
	protected RestAPIRequestImpl request;

	public TestBase() {
		response = new RestAssuredResponseImpl();
		request = new RestAPIRequestImpl();

		FileReader reader;
		try {
			reader = new FileReader(System.getProperty("user.dir")+"\\src\\main\\resources\\config\\config.properties");
			try {
				properties.load(reader);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to convert string to jsonobject
	 * @param obj
	 * @return
	 */
	protected String getJson(Object obj) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(obj);
		JSONObject jsonObj = new JSONObject(json);
		return jsonObj.toString();
	}


}
