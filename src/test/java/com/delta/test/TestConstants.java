package com.delta.test;

import org.apache.http.HttpStatus;

public interface TestConstants {
	public static final int SUCCESS = HttpStatus.SC_OK;
	public static final int CREATED = HttpStatus.SC_CREATED;
}