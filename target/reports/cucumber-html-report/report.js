$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/com/delta/features/Test.feature");
formatter.feature({
  "name": "Test",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Create users in reqres test api",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Create below users \u003cname\u003e and \u003cjob\u003e in reqres api",
  "keyword": "Given "
});
formatter.step({
  "name": "response should be CREATED",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "name",
        "job"
      ]
    },
    {
      "cells": [
        "Test1",
        "job1"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Create users in reqres test api",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Create below users Test1 and job1 in reqres api",
  "keyword": "Given "
});
formatter.match({
  "location": "TestApi.createUsers(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "response should be CREATED",
  "keyword": "Then "
});
formatter.match({
  "location": "TestApi.validateResponse(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Get list of users in regres api",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Get list of users in regres api",
  "keyword": "Given "
});
formatter.match({
  "location": "TestApi.getListOfUsers()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "response should be SUCCESS",
  "keyword": "Then "
});
formatter.match({
  "location": "TestApi.validateResponse(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Get list of users in regres api Negative Case",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Get list of users in regres api",
  "keyword": "Given "
});
formatter.match({
  "location": "TestApi.getListOfUsers()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "response should be CREATED",
  "keyword": "Then "
});
formatter.match({
  "location": "TestApi.validateResponse(String)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: expected:\u003c201\u003e but was:\u003c200\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:645)\r\n\tat org.junit.Assert.assertEquals(Assert.java:631)\r\n\tat com.delta.steps.TestApi.validateResponse(TestApi.java:48)\r\n\tat ✽.response should be CREATED(file:src/test/java/com/delta/features/Test.feature:17)\r\n",
  "status": "failed"
});
formatter.after({
  "status": "passed"
});
});